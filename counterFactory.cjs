function counterFactory(num) {
  //let num = 5;

  function increment() {
    //console.log(num);
    num++;
    return num;
  }
  function decrement() {
    num--;
    return num;
  }
  return { increment, decrement };
}

module.exports = counterFactory;
