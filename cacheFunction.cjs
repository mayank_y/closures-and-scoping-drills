module.exports = function cacheFunction(call_back) {
  let cache = {};

  return function (num) {
    if (cache[num]) {
      console.log("Input already exists. Returning from cache:");
      return cache[num];
    } else {
      console.log();
      let res = call_back(num);
      cache[num] = res;

      return res;
    }
  };
};
