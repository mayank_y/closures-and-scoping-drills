module.exports = function limitFunctionCallCounts(fun1, inp_num) {
  return function one() {
    if (inp_num == 0) {
      return null;
    }
    inp_num--;
    //console.log(inp_num);
    return fun1();
  };
};
