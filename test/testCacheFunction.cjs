let cacheFunction = require("../cacheFunction.cjs");

let result = cacheFunction((a) => {
  return a * a;
});
console.log(result(5));
//result(9);
console.log(result(6));
