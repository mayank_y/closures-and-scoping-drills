let limitFunctionCallCounts = require("../limitFunctionCallCount.cjs");

let result = limitFunctionCallCounts(() => {
  console.log("Hello world");
}, 5);
//console.log(result);
let res = result;
res();
res();
res();
res();
res();
res(); // from here, the function will stop using call back
res(); // and this will not work
