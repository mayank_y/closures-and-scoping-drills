let counterFactory = require("../counterFactory.cjs");

let result = counterFactory(5); //giving 5 as input here only
console.log(result); // this returns the object of closure functions increment and decrement
console.log(result.increment()); // 6
console.log(result.decrement()); //7
